import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import { defaultApi } from "./api";

const increment = function* () {
    const api = defaultApi();

    const productos = yield call(api.getProductos);

    console.log({ productos });
};

export const defaultSaga = function* () {
    yield takeEvery("INCREMENT", increment);
};
