let initialState = {
    number: 1,
};

export const defaultReducer = (
    state = initialState,
    action = { type: null }
) => {
    console.log({ state });
    switch (action.type) {
        case "INCREMENT":
            state = { ...state };
            state.number = state.number + 1;
            return state;

        default:
            state;
            return state;
    }
};
