import { all } from "redux-saga/effects";

import { defaultSaga } from "./default/sagas";

export function* rootSaga() {
    yield all([defaultSaga()]);
}
