import createSagaMiddleware from "redux-saga";
import {
    applyMiddleware,
    Store,
    combineReducers,
    compose,
    createStore as createStoreRedux,
} from "redux";

import { rootReducer } from "./reducers";
import { rootSaga } from "./sagas";

export interface IAppState {
    default: unknown;
}

const sagaMiddleware = createSagaMiddleware();

export function createStore() {
    const store: Store<IAppState> = createStoreRedux(
        rootReducer,
        applyMiddleware(sagaMiddleware)
    );

    sagaMiddleware.run(rootSaga);

    return store;
}
