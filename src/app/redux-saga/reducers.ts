import { combineReducers } from "redux";

import { defaultReducer } from "./default/reducer";

// Define the global store shape by combining our application's
// reducers together into a given structure.
export const rootReducer = combineReducers({
    default: defaultReducer,
});
